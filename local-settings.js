//
// https://support.mozilla.org/fr/kb/configurer-firefox-avec-autoconfig
//

pref("general.config.filename", "firefox.cfg");
pref("general.config.obscure_value", 0);
